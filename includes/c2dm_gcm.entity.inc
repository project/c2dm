<?php

/**
 * Implement hook_entity_info().
 */
function c2dm_entity_info() {
  $entity['c2dm_gcm'] = array(
    'label' => t('Cloud To Device Message'),
    'description' => t('Cloud to device message entity.'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'base table' => 'c2dm_gcm_regid',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'cid',
    ),
    'module' => 'c2dm',
  );

  return $entity;
}

/**
 * Create a new c2dm_gcm object with necessary default value.
 *
 * @return object.
 *   Return an c2dm_entity.
 */
function c2dm_gcm_new($gcm_regid, $uid) {

  if ($c2dm_entity = c2dm_gcm_load_by_user($uid)) {
    return $c2dm_entity;
  }
  else {
    $entity_info = array(
      'gcm_regid' => $gcm_regid,
      'uid' => $uid,
      'created' => REQUEST_TIME,
    );
    return entity_create('c2dm_gcm', $entity_info);
  }
}

/**
 * Save a "c2dm_gcm" entity object.
 *
 * @param object $entity
 *   'c2dm_gcm' object
 *
 * @return mixed.
 *   saved 'c2dm_gcm' object，or FALSE if failed.
 */
function c2dm_gcm_save($entity) {

  if (isset($entity->changed)) {
    $entity->changed = REQUEST_TIME;
  }
  entity_save('c2dm_gcm', $entity);

  return $entity;
}

/**
 * Load a 'c2dm_gcm'entity.
 *
 * @param int $cid
 *   The primary identifier for a c2dm gcm ID.
 *
 * @return mixed
 *   Entity object，or FALSE if failed.
 */
function c2dm_gcm_load($cid) {
  return entity_load_single('c2dm_gcm', $cid);
}


/**
 * Load an array of entity objects keyed by gid.
 *
 * @param mixed $cids
 *   An array of cids, or FALSE to load all entities.
 * @param array $contitions
 *   An array of conditions to filter results.
 * @param boolean $reset
 *   Indicates whether to reset cache.
 *
 * @return mixed
 *   An array of entity objects keyed by gid, or FALSE.
 */
function c2dm_gcm_load_multipal($cids = FALSE, $contitions = array(), $reset = FALSE) {
  return entity_load('c2dm_gcm', $cids, $contitions, $reset);
}

/**
 * Load "c2dm_gcm"entity object by uid.
 *
 * @param int $uid
 *   Owner uid
 *
 * @return mixed
 *   Entity object or FALSE
 */
function c2dm_gcm_load_by_user($uid) {
  $c2dm_gcm = c2dm_gcm_load_multipal(FALSE, array('uid' => $uid));

  return $c2dm_gcm ? reset($c2dm_gcm) : FALSE;
}

/**
 * Delete the "c2dm_gcm" entity object.
 *
 * @param int $cid
 *   分组的ID
 */
function c2dm_gcm_delete($cid) {
  entity_delete('c2dm_gcm', $cid);
}
