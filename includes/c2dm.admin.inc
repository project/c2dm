<?php

/**
 * A form used to obtain Google API Key.
 */
function _c2dm_setting_form($form, &$form_state) {

  $c2dm_settings = variable_get('c2dm_settings', array());

  $form['google_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('GOOGLE API KEY'),
    '#default_value' => isset($c2dm_settings['google_api_key']) ? $c2dm_settings['google_api_key'] : '',
    '#description' => t('Please fill in your google api key'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'save',
  );

  return $form;
}

/**
 * Save the Google API Key in variable 'c2dm_settings'.
 */
function _c2dm_setting_form_submit($form, &$form_state) {
  $c2dm_settings['google_api_key'] = $form_state['input']['google_api_key'];
  variable_set('c2dm_settings', $c2dm_settings);
}
