<?php

/**
 * C2DM module, rules file.
 *
 * @file
 *   C2DM module, rules file.
 */

/**
 * Implements hook_rules_action_info().
 */
function c2dm_rules_action_info() {
  $item = array();
  $item['send_user_notification'] = array(
    'label' => t('Send a message to a user(device)'),
    'group' => t('Cloud to device message'),
    'parameter' => array(
      'user' => array(
        'type' => 'unknown',
        'label' => t('Recipient'),
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message to clients'),
        'description' => t('Fill in your message data to send with json encode. E.g.< {  "demo": "[c2dm-gcm:gcm-regid]"  } >'),
        'restriction' => 'input',
      ),
    ),
  );
  return $item;
}

/**
 * Implements hook_rules_condition_info().
 */
function c2dm_rules_condition_info() {
  $item = array();
  $item['check_user_has_regid'] = array(
    'label' => t('Check whether the user has a registered ID'),
    'group' => t('Cloud to device message'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('Recipient'),
      ),
    ),
  );
  return $item;
}

/**
 * Send user notification.
 *
 * @param Int $user
 *   User ID corresponding to the client.
 *
 * @param Array $message
 *   The message data to send.
 */
function send_user_notification($user, $message) {
  $registatoin_ids = array();
  
  if (is_object($user)) {
    $c2dm = c2dm_gcm_load_by_user($user->uid);
    if ($c2dm) {
      $registatoin_ids[] = $c2dm->gcm_regid;
    }
  }
  elseif (is_array($user)) {
    foreach ($user as $uid) {
      $c2dm = c2dm_gcm_load_by_user($uid);
      if ($c2dm) {
        $registatoin_ids[] = $c2dm->gcm_regid;
      }
    }
  }

  // @TODO message data design.
  $decode_message = drupal_json_decode($message);

  // $message = array("price" => 'send_user_notification');
  send_notification($registatoin_ids, $decode_message);
}

/**
 * Check user has regid.
 *
 * @param Object $user
 *   User ID corresponding to the client.
 *
 * @return boolean
 *   Registered this user or not.
 */
function check_user_has_regid($user) {
  return c2dm_gcm_load_by_user($user->uid) ? TRUE : FALSE;
}
