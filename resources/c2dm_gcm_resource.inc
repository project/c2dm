<?php

/**
 * Access control for the c2dm gcm resource.
 *
 * @param String $op
 *   Option type.
 *
 * @return boolean
 *   Have access or not.
 */
function _c2dm_gcm_resource_access($op) {
  // $op Keep for potential demand.
  if (user_access('register c2dm') || user_access('administer c2dm settings')) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Create uri for c2dm gcm resource.
 * http://yoursite/endpoint/c2dm_gcm   post "gcm_regid", "uid".
 *
 * @param Array $data
 *   Initialize data.
 *
 * @return Object $c2dm_gcm
 *   A c2dm_gcm entity.
 */
function _c2dm_gcm_resource_create($data) {

  if (!isset($data['gcm_regid'])) {
    return services_error(t('gcm_regid is required'), 403);
  }

  if (!isset($data['uid'])) {
    return services_error(t('uid is required'), 403);
  }

  $gcm_regid = $data['gcm_regid'];
  $uid = $data['uid'];

  if (!user_load($uid)) {
    return services_error(t('User @uid could not be found', array('@uid' => $uid)), 404);
  }

  // Determine whether there already has one.
  if ($c2dm_gcm = c2dm_gcm_load_by_user($uid)) {
    $c2dm_gcm->gcm_regid = $gcm_regid;
  }
  else {
    $c2dm_gcm = c2dm_gcm_new($gcm_regid, $uid);
  }
  c2dm_gcm_save($c2dm_gcm);

  return $c2dm_gcm;
}

/**
 * Create uri for c2dm gcm resource.
 * http://yoursite/endpoint/c2dm_gcm/%uid   put "gcm_regid".
 *
 * @param Array $data
 *   Update data.
 *
 * @param Int $uid
 *   Uid from uri.
 *
 * @return Object
 *   Object $c2dm_gcm.
 */
function _c2dm_gcm_resource_update($data, $uid) {
  $c2dm_gcm = c2dm_gcm_load_by_user($uid);

  if ($c2dm_gcm) {
    $c2dm_gcm->gcm_regid = $data['gcm_regid'];
    c2dm_gcm_save($c2dm_gcm);
  }
  else {
    return services_error(t('User @uid could not be found any gcm entity', array('@uid' => $uid)), 404);
  }

  return $c2dm_gcm;
}
